/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.reports.service;

import com.mycompany.reports.dao.SaleDao;
import com.mycompany.reports.model.ReportSale;
import java.util.List;

/**
 *
 * @author roman
 */
public class ReportService {
     public List<ReportSale> getReportSaleByDay(){
        SaleDao dao = new SaleDao();
        return dao.getDayReport();
        
    }
    public List<ReportSale> getReportSaleByMonth(int year){
        SaleDao dao = new SaleDao();
       return dao.getMonthReport(year);
        
    }
}
